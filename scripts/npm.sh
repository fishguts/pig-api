#!/usr/bin/env bash
################################################################################################
# run git command on all of our little buddies
################################################################################################
# set -x

if [[ $# < 1 ]]
then
	echo "Usage: $0 <action> [params]"
	exit 1
elif [[ ! -e ./run.sh ]]
then
	cd ./scripts
fi

if [[ ("$1" = "install" || "$1" = "update") && "$2" = "pig" ]]
then
	# this is a special case we support to force our own "pig" libs to update
	./run.sh rm -rf ./node_modules/pig-core
	./run.sh rm -rf ./node_modules/pig-cmd
	./run.sh rm -rf ./node_modules/pig-quality
	./run.sh npm update
else
	./run.sh npm "${@}"
fi
