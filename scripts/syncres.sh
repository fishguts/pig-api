#!/usr/bin/env bash
####################################################################################################
# synchronizes any resource bits and pieces that we want shared
####################################################################################################

# set -x

if [[ $# < 1 ]]
then
	echo "$0 <project>/src/[select-files] to one or more targets"
	echo "$0 <source=editor|server> [target=editor|server]"
	exit 1
fi

# if we are in the scripts directory then go to parent project
if [[ -e "./syncres.sh" ]]
then
	cd ..
fi

if [[ ! -e "./$1" ]]
then
	echo "Error: cannot find $1"
	exit 1
fi

# the source project from which we will pull source code
SOURCE=$1
# find our targets
if [[ $# > 1 ]]
then
	shift; TARGETS=$@
else
	TARGETS=`echo "editor lib/nodejs server" | sed "s/${SOURCE}//"`
fi


for TARGET in ${TARGETS}
do
	function copyFile {
		echo "copying \"./${SOURCE}/${1}\" to \"./${TARGET}/${1}"\"
		mkdir -p `dirname "./${TARGET}/${1}"`
		cp -R "./${SOURCE}/${1}" "./${TARGET}/${1}"
	}
	copyFile "res/configuration/model-gamuts.yaml"
done
