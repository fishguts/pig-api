#!/usr/bin/env bash
################################################################################################
# run a command and it's params on ever project in our tree
################################################################################################
# set -x

if [[ $# < 1 ]]
then
	echo "Usage: $0 <command> [params]"
	exit 1
elif [[ -e "./run.sh" ]]
then
	cd ..
elif [[ ! -e "./scripts/run.sh" ]]
then
	echo "Error: must be run from project root"
	exit 1
fi

# capture the command and command args so that we may overwrite them
COMMAND=${1}; shift;
PROJECTS="codegen editor lib/nodejs mongo pgsql server"

# if the command we are processing is NOT npm then we include the root otherwise we end up in a recursive loop
if [[ ${COMMAND} != "npm" ]]
then
    PROJECTS=". ${PROJECTS}"
else
	IS_PACKAGE_OPERATION=$(echo "install install-test list ls outdated prune publish rebuild search uninstall unpublish update" | grep -o -w "$1")
	IS_DOCKER_OPERATION=$(echo "${@}" | grep -E "run.*\sdocker:\w+")
	# we don't want to include the root project if this npm command is one that will cause a recursive loop. So we look
	# to see whether it's an known, and likely, npm command and if so then we are good.
	if [[ -n "${IS_PACKAGE_OPERATION}" ]]
	then
	    PROJECTS=". ${PROJECTS}"
   	fi
   	# when it comes to docker we only want to dockerize a subset of our applications. For now it is the guys that are part of our suite.
	if [[ -n "${IS_DOCKER_OPERATION}" ]]
	then
		PROJECTS=$(echo "${PROJECTS}" | sed "s/codegen//g" | sed "s/lib\/nodejs//g")
	fi
fi

# this is totally goofy, but I cannot figure out how to retain quoting otherwise
P=("${1}" "${2}" "${3}" "${4}" "${5}" "${5}" "${6}" "${7}" "${8}" "${9}")
for ((i=${#}; i<10; i++))
do
	unset P[$i]
done


##########################################################################
# apply the command to the project directory in $1
##########################################################################
function _apply {
	# move into the directory and execute the command
	pushd ${1} &> /dev/null
	if [[ "${?}" != "0" ]]
	then
		return
	fi
	echo "${1}: ${COMMAND} ${P[@]}"
	${COMMAND} "${P[@]}"
	echo ""
	popd > /dev/null
}

for PROJECT in ${PROJECTS}
do
	_apply ${PROJECT}
done
