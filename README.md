# Pig API Machine

## Overview

Pig API is a machine which helps one design and generate and build partially to fully functioning APIs. 

## Architecture

The project utilizes a micro-service architecture. It is broken up into:

- **codegen** the brains behind generating projects from API specs.
- **editor** client which a user may use to design and generate API specs (and more).
- **mongo** our document storage for API projects. At the moment we also store project data in it, but this will change.
- **pgsql** postgreSQL storage for API projects.
- **server** the guy in the middle who brings everything together:
	- serves the client
	- builds, polls, starts, stops and tears down projects
	- interfaces with the database.

## Build

The build exists at a **macro** and **micro** level.

The **macro** level is responsible for setting up *pig-API*. Ultimately it will also build, run and tear the whole suite down. But the architecture of this suite is not fully known and this functionality only partially exists right now.  

The **micro** level is the build scripts and CLI that exist within each project.

## Getting Started

1. create a parent directory: `mkdir pig; cd pig`
2. clone the parent project: `git clone https://fishguts@bitbucket.org/fishguts/pig-api.git api`
3. enter pig: `cd api`
4. get his mates: `./scripts/setup.sh env`
5. checkout working: `./scripts/git.sh checkout working`
6. startup the machine. Ultimately this should all be containerized and managed at the **macro** level, but for now you must do the honors:
	1. (in a new terminal window) startup mongo: `cd mongo; npm start`
	2. (in a new terminal window) startup postgresql: `cd pgsql; npm start`
	3. (in a new terminal window) startup the server: `cd server; npm start`
	4. (in a new terminal window) startup the client: `cd client; npm start`

And you should be started. Don't be shy, give it a shot: `http://localhost:9010`